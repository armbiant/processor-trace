# processor-trace

This is the processor trace (PT) support from Intel.
This repo was originally cloned from https://github.com/01org/processor-trace,
with its master branch at commit afbae2983b1978f1f1fe267bfc3fd24a7e41673a.

To see differences with upstream, assuming master is up to date
with origin/master:

bash$ git remote add github https://github.com/01org/processor-trace
bash$ git fetch github
bash$ git checkout master
bash$ git cherry -v github/master

Commits that begin with "-" are ones upstream has and we do not.
Commits that begin with "+" are ones we have and upstream does not.
